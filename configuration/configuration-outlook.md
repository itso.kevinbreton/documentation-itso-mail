# Configuration d'Outlook

Votre messagerie ITSO Mail peut être utilisé depuis un client Microsoft Outlook via un connecteur.
Pour télécharger ce connecteur, cliquer sur le lien ci-dessous (en fonction de votre version: 32 bits ou 64 bits).

[32 bits](https://itso.itsodrive.fr/index.php/s/ipRK7A3pRNwJdeg)

[64 bits](https://itso.itsodrive.fr/index.php/s/AX7QakRkW3N8Gmp)

---

## Installer le connecteur

- Lancez l'installation du connecteur en double cliquant sur le fichier téléchargé

- Cliquez sur **Suivant**

![](../assets/configuration/configuration-outlook1.png)

- Acceptez les termes du contrat de licence, puis cliquez sur **Suivant**

![](../assets/configuration/configuration-outlook2.png)

- Cliquez sur **Suivant** pour lancer l'installation du connecteur

![](../assets/configuration/configuration-outlook3.png)

- Le connecteur est installé, cliquez sur **Fermer**

![](../assets/configuration/configuration-outlook4.png)

---

## Configuration de la boite mail.

- Rendez-vous dans le Panneau de Configuration Windows

![](../assets/configuration/configuration-outlook5.png)

- Cliquez sur Mail (ou Courrier)

![](../assets/configuration/configuration-outlook6.png)

- Cliquez sur le bouton **Ajouter** afin d'ajouter un nouveau profil

![](../assets/configuration/configuration-outlook7.png)

- Nommez votre nouveau profil (exemple: ITSO Mail), puis cliquez sur **OK**

![](../assets/configuration/configuration-outlook8.png)

- Sélectionnez **Configuration manuelle ou types de serveurs supplémentaires**, puis cliquez sur **Suivant**

![](../assets/configuration/configuration-outlook9.png)

- Sélectionnez **Autre**, puis **Zimbra Collaboration Server**, puis cliquez sur **Suivant**

![](../assets/configuration/configuration-outlook10.png)

- Renseignez l'adresse email et le mot de passe associé à votre compte ITSO Mail, puis cliquez sur **OK**

![](../assets/configuration/configuration-outlook11.png)

- Une notification vous incite à redémarrarer Outlook pour appliquer les changements, cliquez sur **OK**

![](../assets/configuration/configuration-outlook12.png)

- La configuration est terminée, cliquez sur **Terminer**

![](../assets/configuration/configuration-outlook13.png)

- Le profil Outlook est configuré, pour l'utiliser de manière permanante, sélectionnez **Toujours utiliser ce profil** et sélectionner le profil crée précédement. Pour terminer, cliquez sur **OK**

![](../assets/configuration/configuration-outlook14.png)

- Lancez Outlook afin de valider le bon fonctionnement de votre boite ITSO Mail.