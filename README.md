<h1 align="center">Documentation ITSO Mail</h1>

<div style="text-align:center"><img src ="../assets/accueil/logo-itso-mail.png" /></div>
---

Bienvenue sur notre documentation utilisateur ITSO Mail.
Pour commencer la navigation, veuillez utiliser le menu de gauche.
Vous pouvez également effectuer une recherche depuis la barre située en haut à gauche de la page.

---

### Questions ou remarques
Pour toute question ou remarque en rapport avec cette documentation, vous pouvez nous contacter sur notre adresse email:

<support@itso.fr>

Toute l'équipe d'ITSO Technologies vous souhaite une agréable journée.